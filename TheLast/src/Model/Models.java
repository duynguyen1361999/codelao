package Model;
import javax.swing.table.DefaultTableModel;

import Connector.Connect;
import View.Login;

import java.io.*;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

public class Models {
	private String date ;
    public  DefaultTableModel getInfo(){
        try{
            String sql = "SELECT * FROM staff WHERE id = ";
            sql = sql + Integer.toString(Login.getId());
            System.out.println(sql);
            Connect conn = new Connect();
            ResultSet res =  conn.creQuery(sql);

            return buildTableModel(res);

        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public boolean changeInfo(String name, String email, String phone){
        try{
            String sql = "UPDATE staff SET name = " + "\"" + name + "\", email = " + "\"" + email + "\", phonenumber = " + "\"" + phone + "\" WHERE id = " + Login.getId();
            Connection conn = new Connect().connect();
            Statement stmt = conn.createStatement();
            stmt.executeUpdate(sql);

            return true;
        } catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public  boolean changePass(String pass, String cfpass) {
        if (pass.compareTo(cfpass) != 0) return  false;

        String sql = "UPDATE user SET password = " + pass +" WHERE id = " + Integer.toString(Login.getId());
        try {
            Connect conn = new Connect();
//            System.out.println(sql);
            Statement stmt  = conn.connect().createStatement();
            stmt.executeUpdate(sql);
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        return true;
    }

    public  boolean register(String username, String pass, String fullName, String email, String phone){
        try{
            Connect conn = new Connect();

            String sql = "SELECT username FROM user WHERE username = " + "\""+ username +"\"" ;
            ResultSet res = conn.creQuery(sql);
            if (res.next()) return false;

            sql = "INSERT INTO user(username,password) VALUE("+ "\""+username +"\""+ "," +"\""+  pass +"\""+ ")";
            Statement stmt = conn.connect().createStatement();
            stmt.executeUpdate(sql);

            sql = "SELECT id,username FROM user WHERE username = " + "\""+ username +"\"" ;
            res = conn.creQuery(sql);
            res.next();
            int id = res.getInt("id");
            sql = "INSERT INTO staff(id,name,email,phonenumber) VALUE("+Integer.toString(id) + "," +"\""+  fullName  +"\""+ ","+"\""+  email +"\""+ "," +"\""+ phone +"\""+ ")";
            stmt = conn.connect().createStatement();
            stmt.executeUpdate(sql);
        } catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean delUser(String username){
        try{
            Connect conn = new Connect();
            String sql = "SELECT id,username FROM user WHERE username = " + "\""+ username +"\"";
            ResultSet res = conn.creQuery(sql);
            if (!res.next()) return false;



            sql = "DELETE FROM staff WHERE id = " + Integer.toString(res.getInt("id"));
            Statement stmt = conn.connect().createStatement();
            stmt.executeUpdate(sql);
            
            sql = "DELETE FROM user WHERE id = " + Integer.toString(res.getInt("id"));
            stmt = conn.connect().createStatement();
            stmt.executeUpdate(sql);

        }catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public DefaultTableModel buildTableModel(ResultSet rs)
            throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<String>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<Vector<Object>>();
        while (rs.next()) {
            Vector<Object> vector = new Vector<Object>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }

        return new DefaultTableModel(data, columnNames);

    }

    public DefaultTableModel getInfoAllUsers(){
        try {
            String sql = "SELECT user.id, username, name, email, phonenumber, permission FROM user JOIN staff ON user.id = staff.id";
            ResultSet res = new Connect().creQuery(sql);
            return buildTableModel(res);

        } catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
    
    public DefaultTableModel checkStorage(){
        try {
            String sql = "SELECT TenThuoc, DonVi, MaLo, SoLuong, HSD FROM thuoc JOIN lothuoc ON thuoc.MaThuoc = lothuoc.MaThuoc";
            ResultSet res = new Connect().creQuery(sql);
            return buildTableModel(res);

        } catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
    
    public DefaultTableModel getMedicineTable(){
        try {
            String sql = "SELECT * FROM thuoc";
            ResultSet res = new Connect().creQuery(sql);
            return buildTableModel(res);

        } catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public DefaultTableModel checkAllLog() {
        try{
            String sql = "SELECT * FROM history";
            ResultSet res = new Connect().creQuery(sql);

            return buildTableModel(res);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public DefaultTableModel checkOneLog(int idL){
        try{
            String sql = "SELECT * FROM history WHERE id = " + Integer.toString(idL);

            ResultSet rs = new Connect().creQuery(sql);

            return  buildTableModel(rs);
        } catch (Exception e)
        {
            e.printStackTrace();
            return  null;
        }
    }

    public ResultSet checkInfoThuoc(String id){
        try{
            String sql = "SELECT * FROM thuoc WHERE MaThuoc = " + id;

            ResultSet res = new Connect().creQuery(sql);
            return  res;
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public  ResultSet getListthuoc(ArrayList<String> listId){
        try {
            String sql = "SELECT * FROM thuoc WHERE MaThuoc = ";
            for(int i = 0; i < listId.size()-1; i++){
                sql = sql + listId.get(i) + " OR MaThuoc = ";
            }
            sql = sql + listId.get(listId.size()-1);

            return new Connect().creQuery(sql);
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public DefaultTableModel previewBill (ArrayList<String> listId){
        try{
            ResultSet res = getListthuoc(listId);

            return buildTableModel(res);
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public boolean createHistory(ArrayList<String> listID, String cus, int sum){
        try{
            String idList = "";

            DateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd");
            Calendar cal = Calendar.getInstance();

            date = dateFormat.format(cal.getTime());
            for (String x : listID) idList = idList + " " + x;

            String sql = "INSERT INTO history(dsthuoc,tien,ngay,customer,idU) VALUE( "  + "'" +idList +"'" + ", " + "'" +Integer.toString(sum)+ "'" +", " + "'" + date + "'" + ", " + "'" + cus + "'" + ", " + "'" + Login.getId() + "'" + ")";
            Connection conn = new Connect().connect();
            Statement stmt = conn.createStatement();
            stmt.executeUpdate(sql);

            return true;
        } catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }

    public boolean addThuoc(String ten, String loai, String donvi, String gia){
        try{
            String sql = "INSERT INTO thuoc(TenThuoc, LoaiThuoc, DonVi, GiaTien) VALUE(" + "\""+ ten + "\", \"" + loai + "\" , \"" + donvi + "\", " + gia + ")";

            Connection conn = new Connect().connect();
            Statement stmt = conn.createStatement();
            stmt.executeUpdate(sql);

            return true;
        } catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }

    public boolean addLo(String malo, String mathuoc, String hsd, String soluong){
        try{
            DateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd");
            Calendar cal = Calendar.getInstance();
            String sql = "INSERT INTO lothuoc(Malo, MaThuoc, HSD, SoLuong, NgayNhap) VALUE(" +"\"" + malo +"\", " + "\"" + mathuoc + "\", \"" + hsd + "\", " + soluong + ", \"" + dateFormat.format(cal.getTime()) +"\")";
            Connection conn = new Connect().connect();
            Statement stmt = conn.createStatement();

            stmt.executeUpdate(sql);

            return true;
        } catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }

    public void printBills(ArrayList<String> idList, int[] amount, String cus,String date, int sum){
        DefaultTableModel res = previewBill(idList);
        String num = "So Luong";
        res.addColumn(num);
        for (int i = 0; i < idList.size(); i++){
            res.setValueAt(new Integer(amount[i]), i, 5);
        }
        try{
            BufferedWriter bw = new BufferedWriter(new FileWriter( new File("bill.txt")));
            bw.write("Bill \r\n");
            bw.write("Thuoc: ");
            for (int i = 0; i < res.getRowCount(); i++){
                String name = new String(res.getValueAt(i,1).toString());
                bw.write(name + " x " + new String(res.getValueAt(i,5).toString()) + "\r\n");
            }
            bw.write("Ngay: " + date + "\r\n");
            bw.write("Khach hang: " + cus + "\r\n");
            bw.write("Total Money: " + sum);
            bw.close();
        } catch (Exception e){
            e.printStackTrace();
        }
        for(int i =0; i< idList.size(); i++) {
        	
        
        String sql = "UPDATE lothuoc Set SoLuong = SoLuong - " + res.getValueAt(i, 5).toString() + " WHERE MaThuoc = " + idList.get(i);
        Connection conn;
		try {
			conn = new Connect().connect();
			Statement stmt = conn.createStatement();

	        stmt.executeUpdate(sql);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        }
    }

}
