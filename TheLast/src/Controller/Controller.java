package Controller;

import javax.swing.table.DefaultTableModel;

import Model.Models;
import View.Login;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;

public class Controller {
    Models models = new Models();

    public Controller(){

    }


    public void run() {
        new Login();
    }

    public  DefaultTableModel infoAccount(){
        return  models.getInfo();
    }

    public boolean changeInfo(String name, String email, String phone){
        return models.changeInfo(name, email, phone);
    }

    public  boolean changePass(String pass, String cfpass){
        return  models.changePass(pass, cfpass);
    }

    public  boolean register(String username, String pass, String fullName, String email, String phone){
        return models.register(username, pass, fullName, email, phone);
    }

    public boolean delUser(String uName) {
        return models.delUser(uName);
    }

    public DefaultTableModel getInfoAllUsers() {
        return  models.getInfoAllUsers();
    }
    
    public DefaultTableModel getMedicineTable() {
        return  models.getMedicineTable();
    }

    public  DefaultTableModel checkAllLog(){
        return models.checkAllLog();
    }

    public  DefaultTableModel checkOneLog(int idL){
        return  models.checkOneLog(idL);
    }

    public  ResultSet checkInfoThuoc(String id) {
        return models.checkInfoThuoc(id);
    }
    public DefaultTableModel buildTableModel(ResultSet res) throws Exception{
        return models.buildTableModel(res);
    }
    public DefaultTableModel checkStorage() {
    	return models.checkStorage();
    }
    

    public DefaultTableModel previewBill(ArrayList<String> listId){
        return models.previewBill(listId);
    }

    public boolean  createHistory(ArrayList<String> idList, String cus, int sum){
        return models.createHistory(idList, cus, sum);
    }

    public boolean addThuoc(String ten, String loai, String donvi, String gia){
        return models.addThuoc(ten, loai, donvi, gia);
    }

    public boolean addLo(String malo, String mathuoc, String hsd, String soluong){
        return models.addLo(malo, mathuoc, hsd, soluong);
    }

    public void printBills(ArrayList<String> idList, int[] amount, String cus,String date, int sum)
    {
        models.printBills(idList, amount, cus,date, sum);
    }
}
