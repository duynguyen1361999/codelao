package Connector;

import javax.xml.transform.Result;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Connect {
    public  Connection connect() throws Exception
    {
        Connection conn = DriverManager.getConnection (
                "jdbc:mysql://localhost/medicinestore",
                "root",
                ""
        );

        return  conn;
    }

    public ResultSet creQuery(String sql) throws  Exception{
        Connect conn = new Connect();
        Statement stmt = conn.connect().createStatement();
        ResultSet res = stmt.executeQuery(sql);

        return res;
    }


    public  Connect() throws  Exception{

    }

    public  Connect(String url, String user, String password) throws  Exception{

    }
}
