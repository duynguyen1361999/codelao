package View;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import Controller.Controller;

public class CheckMed {
	 Controller controller = new Controller();
	 CheckMed(){
	        DefaultTableModel res = controller.getMedicineTable();

	        JOptionPane.showMessageDialog(null, new JScrollPane(new JTable(res)));
	    }
}
