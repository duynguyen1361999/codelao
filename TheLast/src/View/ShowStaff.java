package View;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import Controller.Controller;

import java.sql.ResultSet;

public class ShowStaff  {
    Controller controller = new Controller();
    ShowStaff(){
        DefaultTableModel res = controller.getInfoAllUsers();

        JOptionPane.showMessageDialog(null, new JScrollPane(new JTable(res)));
    }
}
