package View;

import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import Controller.Controller;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class History {
    Controller controller = new Controller();
    History(){
        JFrame aFrame = new JFrame();

        aFrame.setVisible(true);
        aFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        aFrame.setTitle("Check Log");
        aFrame.setBounds(100, 100, 536, 305);
        aFrame.setLocationRelativeTo(null);
        aFrame.setResizable(false);
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		aFrame.setContentPane(contentPane);
		
		JButton check1 = new JButton("Check 1");
		check1.setForeground(Color.MAGENTA);
		check1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		
		JButton checkAll = new JButton("Check all");
		checkAll.setForeground(Color.BLUE);
		checkAll.setFont(new Font("Tahoma", Font.PLAIN, 18));
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(170)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(checkAll, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(check1, GroupLayout.PREFERRED_SIZE, 175, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(165, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(49)
					.addComponent(check1, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
					.addGap(49)
					.addComponent(checkAll, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(79, Short.MAX_VALUE))
		);
		contentPane.setLayout(gl_contentPane);

//        final JPanel contentPanel = new JPanel();
//        aFrame.setContentPane(contentPanel);
//
//        JButton checkAllLog = new JButton("Check All Log");
		checkAll.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                DefaultTableModel res = controller.checkAllLog();

                JOptionPane.showMessageDialog(null, new JScrollPane(new JTable(res)));
            }
        });
//        contentPanel.add(checkAllLog);
//
//        JButton checkOneLog = new JButton("Check 1 Log");
        check1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                String input;
                input = JOptionPane.showInputDialog(new JTextField(), "Search");
                try{
                    int id = Integer.parseInt(input);
                    DefaultTableModel res = controller.checkOneLog(id);

                    JOptionPane.showMessageDialog(null, new JScrollPane(new JTable(res)));
                } catch (Exception e)
                {
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(new JFrame(), "Can not found id");
                }
//                System.out.println(id);

            }
        });
        
        ImageIcon background=new ImageIcon("others.jpg");
	    Image img=background.getImage();
	    Image temp=img.getScaledInstance(526,295,Image.SCALE_SMOOTH);
	    background=new ImageIcon(temp);
	    JLabel back=new JLabel(background);
	    back.setLayout(null);
	    back.setBounds(0,0,526,295);
		contentPane.add(back);
//        contentPanel.add(checkOneLog);
    }
}
