package View;

import javax.swing.*;

import Controller.Controller;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class DelUser extends JFrame {
    Controller controller = new Controller();
    DelUser(){
    	this.setVisible(true);
    	this.setSize(300, 150);
    	this.setTitle("Delete user");
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        final JPanel contentPanel = new JPanel();

        final JTextField username = new JTextField();
        username.setPreferredSize(new Dimension(200,24));

        JButton delUser = new JButton("Del");
        delUser.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                String uName = username.getText();
                if (controller.delUser(uName)){
                    JOptionPane.showMessageDialog(new JFrame(), "Done!!");
                    username.setText("");
                } else JOptionPane.showMessageDialog(new JFrame(), "Can't found user " + uName);
            }
        });

        contentPanel.add(new JLabel("Delete User : "));
        contentPanel.add(username);
        contentPanel.add(delUser);
        this.setContentPane(contentPanel);
        
        ImageIcon background=new ImageIcon("others.jpg");
	    Image img=background.getImage();
	    Image temp=img.getScaledInstance(526,432,Image.SCALE_SMOOTH);
	    background=new ImageIcon(temp);
	    JLabel back=new JLabel(background);
	    back.setLayout(null);
	    back.setBounds(0,0,290,140);
		contentPanel.add(back);
       
    }
}
