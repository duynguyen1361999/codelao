package View;



import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import Controller.Controller;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class AddLo extends JFrame {
	private JPanel contentPane;
	private JTextField malo;
	private JLabel iLo;
	private JTextField loaithuoc;
	private JLabel iMathuoc;
	private JButton addBtn;
	private JLabel iDay;
	private JTextField day;
	private JLabel iMonth;
	private JTextField month;
	private JLabel iYear;
	private JTextField year;
	private JLabel iMount;
	private JTextField mount;
    AddLo(){
        final Controller controller = new Controller();

        this.setVisible(true);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setBounds(100, 100, 635, 484);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.setContentPane(contentPane);
		
		iLo = new JLabel("Ma lo");
		iLo.setFont(new Font("Tahoma", Font.PLAIN, 18));
		
		malo = new JTextField();
		malo.setColumns(10);
		
		iMathuoc = new JLabel("Ma thuoc");
		iMathuoc.setFont(new Font("Tahoma", Font.PLAIN, 18));
		
		loaithuoc = new JTextField();
		loaithuoc.setColumns(10);
		
		addBtn = new JButton("Add");
		addBtn.setForeground(Color.BLUE);
		addBtn.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		iDay = new JLabel("Day");
		iDay.setFont(new Font("Tahoma", Font.PLAIN, 18));
		
		day = new JTextField();
		day.setColumns(10);
		
		iMonth = new JLabel("Month");
		iMonth.setFont(new Font("Tahoma", Font.PLAIN, 18));
		
		month = new JTextField();
		month.setColumns(10);
		
		iYear = new JLabel("Year");
		iYear.setFont(new Font("Tahoma", Font.PLAIN, 18));
		
		year = new JTextField();
		year.setColumns(10);
		
		iMount = new JLabel("Mount");
		iMount.setFont(new Font("Tahoma", Font.PLAIN, 18));
		
		mount = new JTextField();
		mount.setColumns(10);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(262)
					.addComponent(addBtn)
					.addContainerGap(289, Short.MAX_VALUE))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(22)
							.addComponent(iDay, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(day, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED, 33, Short.MAX_VALUE)
							.addComponent(iMonth)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(month, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
							.addGap(49)
							.addComponent(iYear, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(year, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
							.addGap(23))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addContainerGap(109, Short.MAX_VALUE)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
								.addComponent(iLo, Alignment.LEADING)
								.addComponent(iMathuoc, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
							.addGap(55)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
								.addComponent(loaithuoc)
								.addComponent(malo, GroupLayout.DEFAULT_SIZE, 172, Short.MAX_VALUE))))
					.addGap(36)
					.addComponent(iMount, GroupLayout.PREFERRED_SIZE, 62, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(mount, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
					.addGap(46))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(85)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(iLo)
						.addComponent(malo, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE))
					.addGap(21)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(iMathuoc)
						.addComponent(loaithuoc, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE))
					.addGap(61)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(iDay)
						.addComponent(month, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
						.addComponent(iMonth)
						.addComponent(day, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
						.addComponent(mount, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
						.addComponent(iMount)
						.addComponent(year, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
						.addComponent(iYear))
					.addPreferredGap(ComponentPlacement.RELATED, 83, Short.MAX_VALUE)
					.addComponent(addBtn)
					.addGap(59))
		);
		contentPane.setLayout(gl_contentPane);

        addBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                String hsd = year.getText()+ "-" + month.getText() + "-" + day.getText();
                if(controller.addLo(malo.getText(), loaithuoc.getText(), hsd, mount.getText())) {
                	JOptionPane.showMessageDialog(new JFrame(), "Done");
                }else {
                	JOptionPane.showMessageDialog(new JFrame(), "Lỗi: trùng mã lô thuốc hoặc mã thuốc không tồn tại");
                }
            }
        });
        
        ImageIcon background=new ImageIcon("others.jpg");
	    Image img=background.getImage();
	    Image temp=img.getScaledInstance(625,474,Image.SCALE_SMOOTH);
	    background=new ImageIcon(temp);
	    JLabel back=new JLabel(background);
	    back.setLayout(null);
	    back.setBounds(0,0,625,474);
		contentPane.add(back);
//        contenPanel.add(addLo);
    }
}

