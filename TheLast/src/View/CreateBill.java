package View;

import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import Connector.Connect;
import Controller.Controller;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class CreateBill extends JFrame {
    Controller controller = new Controller();
    private ArrayList<String> idThuoc = new ArrayList<String>();
    private int[] amount = new int[1005];
    private int sum = 0;
    private int n = 0;
    private String cus;
    private String date;
	


    CreateBill(){
        this.setTitle("Create Bill");
//        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        this.setVisible(true);
         
        	
        
       
        
//        this.setSize(300,400);
//
//        final JPanel contentPanel = new JPanel();
//
//        this.setContentPane(contentPanel);
//
//
//        JButton insertMedicine = new JButton("Insert Medicine ...");
//        contentPanel.add(insertMedicine);
        sum = 0;
        setBounds(100, 100, 536, 442);
        setLocationRelativeTo(null);
		setResizable(false);
        JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JButton insertMedicine = new JButton("Insert medicine");
		insertMedicine.setForeground(Color.BLUE);
		insertMedicine.setFont(new Font("Tahoma", Font.PLAIN, 18));
		
		JButton previewBill = new JButton("Preview bill");
		previewBill.setFont(new Font("Tahoma", Font.PLAIN, 18));
		previewBill.setForeground(Color.RED);
		
		JButton printBill = new JButton("Print bill");
		printBill.setFont(new Font("Tahoma", Font.PLAIN, 18));
		printBill.setForeground(Color.GREEN);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
					.addContainerGap(154, Short.MAX_VALUE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
						.addComponent(printBill, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(previewBill, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(insertMedicine, GroupLayout.DEFAULT_SIZE, 219, Short.MAX_VALUE))
					.addGap(137))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(61)
					.addComponent(insertMedicine, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
					.addGap(52)
					.addComponent(previewBill, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
					.addGap(57)
					.addComponent(printBill, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(72, Short.MAX_VALUE))
		);
		contentPane.setLayout(gl_contentPane);

        insertMedicine.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                String idx;
                String sl;
                JTextField amountText = new JTextField();
                
                try{
                    idx = JOptionPane.showInputDialog(new JTextField(), "Input ID Medicine");

                    ResultSet res = controller.checkInfoThuoc(idx);
                    
                    if(res.next()) {
                    	
                    
                    
                    
                    String sql;
        			sql = "SELECT SoLuong FROM lothuoc WHERE MaThuoc = " + idx;
        			Statement compare = null;
        			Connection con;
					
					con = new Connect().connect();
					
        			compare = con.createStatement();
        			ResultSet rs = compare.executeQuery(sql);
        			int soluong = 0;
        			while(rs.next()) {
        				soluong = rs.getInt("SoLuong");
        				System.out.println(soluong);
        			}
                    sl = JOptionPane.showInputDialog(amountText, "Input Amount Medicine");
                    if( Integer.parseInt(sl) > soluong && soluong > 0) {
                    	JOptionPane.showMessageDialog(contentPane, "The quantity requested is greater than the remainder in the lot", "Error", JOptionPane.ERROR_MESSAGE);
                    	
                    }
                    else if(soluong == 0) {
                    	JOptionPane.showMessageDialog(contentPane, "The remainder in the plot is zero", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                    else {	
                    cus = JOptionPane.showInputDialog(new JTextField(), "Customer's name");
                    
                    }
                    boolean flag = true;
                    for (int i = 0; i < idThuoc.size(); i++){
                        String x = idThuoc.get(i);
                        if (x.compareTo(idx) == 0){
                            flag = false;
                            amount[i] += Integer.parseInt(sl);
                        }
                    }

                    if (flag){
                        idThuoc.add(idx);
                        amount[idThuoc.size()-1] += Integer.parseInt(sl);
                    }



                    sum += res.getInt("GiaTien") * Integer.parseInt(sl);
                    
                    }else {
                    	JOptionPane.showMessageDialog(contentPane, "Not found ID", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                } catch (NullPointerException e)
                {
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(new JFrame(), "Not found ID");
                }
                catch (SQLException e)
                {
                    e.printStackTrace();
                }
                catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

            }
        });

//        JButton previewBill = new JButton("Preview Bill");
        contentPane.add(previewBill);
        previewBill.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {

                DefaultTableModel res = controller.previewBill(idThuoc);
                String num = "So Luong";
                String guest = "Customer";
                res.addColumn(num);
                res.addColumn(guest);
                for (int i = 0; i < idThuoc.size(); i++){
                    res.setValueAt(new Integer(amount[i]), i, 5);
                    res.setValueAt(cus, i, 6);
                }
                
                JOptionPane.showMessageDialog(null, new JScrollPane(new JTable(res)));
                JOptionPane.showMessageDialog(new JFrame(), "Total money : " + sum);
            }
        });

//        JButton createBill = new JButton("Create Bill");
        contentPane.add(printBill);
        printBill.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
            	DateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd");
                Calendar cal = Calendar.getInstance();

                date = dateFormat.format(cal.getTime());
                if (controller.createHistory(idThuoc, cus, sum )){
                    JOptionPane.showMessageDialog(new JFrame(), "Done!!");
                    controller.printBills(idThuoc, amount, cus, date, sum);
                }   else JOptionPane.showMessageDialog(new JFrame(), "Error!!");
            }
        });
        
        ImageIcon background=new ImageIcon("others.jpg");
	    Image img=background.getImage();
	    Image temp=img.getScaledInstance(526,432,Image.SCALE_SMOOTH);
	    background=new ImageIcon(temp);
	    JLabel back=new JLabel(background);
	    back.setLayout(null);
	    back.setBounds(0,0,526,432);
		contentPane.add(back);
		
		
//		this.addWindowListener(new WindowAdapter() {
//			public void windowClosing(WindowEvent e) {
//				
//				Viewer view = new Viewer();
//				view.setEnabled(true);
//			}
//		});
    }
}
