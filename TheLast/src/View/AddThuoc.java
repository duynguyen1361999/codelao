package View;



import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

import Controller.Controller;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AddThuoc extends JFrame {
	
	private JPanel contentPane;
	private JLabel iTen;
	private JTextField tenField;
	private JLabel iLoai;
	private JTextField loaiField;
	private JLabel iUnit;
	private JTextField unitField;
	private JLabel iCost;
	private JTextField costField;
	private JButton addBtn;
    public  AddThuoc(){
        final Controller controller = new Controller();

//        this.setSize(300, 400);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setVisible(true);
        this.setTitle("Add Thuoc");
        this.setBounds(100, 100, 560, 483);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        
        
        
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		iTen = new JLabel("TenThuoc");
		iTen.setFont(new Font("Tahoma", Font.PLAIN, 18));
		
		tenField = new JTextField();
		tenField.setColumns(10);
		
		
		iLoai = new JLabel("LoaiThuoc");
		iLoai.setFont(new Font("Tahoma", Font.PLAIN, 18));
		
		loaiField = new JTextField();
		loaiField.setColumns(10);
		
		
		iUnit = new JLabel("DonVi");
		iUnit.setFont(new Font("Tahoma", Font.PLAIN, 18));
		
		
		unitField = new JTextField();
		unitField.setColumns(10);
	
		
		iCost = new JLabel("Gia");
		iCost.setFont(new Font("Tahoma", Font.PLAIN, 18));
		
		costField = new JTextField();
		costField.setColumns(10);
		
		
		addBtn = new JButton("Add");
		addBtn.setForeground(Color.BLUE);
		addBtn.setFont(new Font("Tahoma", Font.PLAIN, 16));
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(88)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
						.addComponent(iCost, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(iUnit, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(iTen, Alignment.LEADING)
						.addComponent(iLoai, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
					.addGap(43)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(costField, GroupLayout.DEFAULT_SIZE, 239, Short.MAX_VALUE)
						.addComponent(unitField)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
							.addComponent(loaiField)
							.addComponent(tenField, GroupLayout.DEFAULT_SIZE, 239, Short.MAX_VALUE)))
					.addGap(82))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(236)
					.addComponent(addBtn)
					.addContainerGap(239, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(85)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(tenField, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
						.addComponent(iTen))
					.addGap(30)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(loaiField, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
						.addComponent(iLoai))
					.addGap(28)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(iUnit)
						.addComponent(unitField, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE))
					.addGap(31)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(iCost)
						.addComponent(costField, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE))
					.addGap(53)
					.addComponent(addBtn)
					.addContainerGap(52, Short.MAX_VALUE))
		);
		contentPane.setLayout(gl_contentPane);


        addBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                String ten = tenField.getText();
                String loai = loaiField.getText();
                String donvi = unitField.getText();
                String gia = costField.getText();

                if (controller.addThuoc(ten, loai, donvi, gia)){
                    JOptionPane.showMessageDialog(new JFrame(), "Done");
                } else JOptionPane.showMessageDialog(new JFrame(), "Error");
            }
        });
        
        ImageIcon background=new ImageIcon("others.jpg");
	    Image img=background.getImage();
	    Image temp=img.getScaledInstance(550,473,Image.SCALE_SMOOTH);
	    background=new ImageIcon(temp);
	    JLabel back=new JLabel(background);
	    back.setLayout(null);
	    back.setBounds(0,0,550,473);
		contentPane.add(back);

        
    }
}
