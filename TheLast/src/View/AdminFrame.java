package View;

import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.border.EmptyBorder;

import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AdminFrame extends JFrame {
    AdminFrame(){
    	this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
      	 this.setTitle("Administrator");
           this.setVisible(true);
           setResizable(false);
           
      	setBounds(100, 100, 481, 508);
      	setLocationRelativeTo(null);
   		JPanel contentPane = new JPanel();
   		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
   		setContentPane(contentPane);
   		
   		JButton addUser = new JButton("Add User");
   		addUser.setFont(new Font("Tahoma", Font.PLAIN, 18));
   		
   		JButton showUser = new JButton("Show Staff");
   		showUser.setFont(new Font("Tahoma", Font.PLAIN, 18));
   		
   		JButton delUser = new JButton("Delete User");
   		delUser.setFont(new Font("Tahoma", Font.PLAIN, 18));
   		GroupLayout gl_contentPane = new GroupLayout(contentPane);
   		gl_contentPane.setHorizontalGroup(
   			gl_contentPane.createParallelGroup(Alignment.LEADING)
   				.addGroup(gl_contentPane.createSequentialGroup()
   					.addGap(160)
   					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
   						.addComponent(addUser, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 161, Short.MAX_VALUE)
   						.addComponent(showUser, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 161, Short.MAX_VALUE)
   						.addComponent(delUser, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 161, Short.MAX_VALUE))
   					.addGap(147))
   		);
   		gl_contentPane.setVerticalGroup(
   			gl_contentPane.createParallelGroup(Alignment.LEADING)
   				.addGroup(gl_contentPane.createSequentialGroup()
   					.addGap(111)
   					.addComponent(addUser)
   					.addGap(69)
   					.addComponent(showUser)
   					.addGap(66)
   					.addComponent(delUser)
   					.addContainerGap(130, Short.MAX_VALUE))
   		);
   		contentPane.setLayout(gl_contentPane);
          addUser.addActionListener(new ActionListener() {
              public void actionPerformed(ActionEvent actionEvent) {
                  new AddUser();
              }
          });


          delUser.addActionListener(new ActionListener() {
              public void actionPerformed(ActionEvent actionEvent) {
                  new DelUser();
              }
          });


          showUser.addActionListener(new ActionListener() {
              public void actionPerformed(ActionEvent actionEvent) {
                  new ShowStaff();
              }
          });

          ImageIcon background=new ImageIcon("others.jpg");
   	    Image img=background.getImage();
   	    Image temp=img.getScaledInstance(471,498,Image.SCALE_SMOOTH);
   	    background=new ImageIcon(temp);
   	    JLabel back=new JLabel(background);
   	    back.setLayout(null);
   	    back.setBounds(0,0,471,498);
   		contentPane.add(back);

       
    }
}
