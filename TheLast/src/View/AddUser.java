package View;

import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.border.EmptyBorder;

import Controller.Controller;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AddUser extends JFrame {
	private JPanel contentPane;
	private JButton regBtn;
	private JLabel iUsername;
	private JTextField username;
	private JLabel iPassword;
	private JLabel iFullname;
	private JTextField fullname;
	private JLabel iEmail;
	private JTextField email;
	private JLabel iPhone;
	private JTextField phone;
	private JPasswordField pass;
    Controller controller = new Controller();
    AddUser(){
    	 this.setVisible(true);
    	 this.setTitle("Register");

    	 setBounds(100, 100, 635, 484);
    	 setLocationRelativeTo(null);
    	 setResizable(false);
 		contentPane = new JPanel();
 		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
 		setContentPane(contentPane);
 		
 		regBtn = new JButton("Register");
 		regBtn.setForeground(Color.BLUE);
 		regBtn.setFont(new Font("Tahoma", Font.PLAIN, 18));
 		
 		iUsername = new JLabel("Username");
 		iUsername.setFont(new Font("Tahoma", Font.PLAIN, 18));
 		
 		username = new JTextField();
 		username.setColumns(10);
 		
 		iPassword = new JLabel("Password");
 		iPassword.setFont(new Font("Tahoma", Font.PLAIN, 18));
 		
 		iFullname = new JLabel("Full name");
 		iFullname.setFont(new Font("Tahoma", Font.PLAIN, 18));
 		
 		fullname = new JTextField();
 		fullname.setColumns(10);
 		
 		iEmail = new JLabel("Email");
 		iEmail.setFont(new Font("Tahoma", Font.PLAIN, 18));
 		
 		email = new JTextField();
 		email.setColumns(10);
 		
 		iPhone = new JLabel("Phone");
 		iPhone.setFont(new Font("Tahoma", Font.PLAIN, 18));
 		
 		phone = new JTextField();
 		phone.setColumns(10);
 		
 		pass = new JPasswordField();
 		GroupLayout gl_contentPane = new GroupLayout(contentPane);
 		gl_contentPane.setHorizontalGroup(
 			gl_contentPane.createParallelGroup(Alignment.TRAILING)
 				.addGroup(gl_contentPane.createSequentialGroup()
 					.addGap(122)
 					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
 						.addComponent(iPassword, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
 						.addComponent(iUsername, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
 						.addGroup(Alignment.LEADING, gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
 							.addComponent(iEmail, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
 							.addComponent(iFullname, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
 							.addComponent(iPhone, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 63, GroupLayout.PREFERRED_SIZE)))
 					.addGap(29)
 					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
 						.addComponent(phone, GroupLayout.DEFAULT_SIZE, 221, Short.MAX_VALUE)
 						.addComponent(pass, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 221, Short.MAX_VALUE)
 						.addComponent(email, GroupLayout.DEFAULT_SIZE, 221, Short.MAX_VALUE)
 						.addComponent(fullname, GroupLayout.DEFAULT_SIZE, 221, Short.MAX_VALUE)
 						.addComponent(username, GroupLayout.DEFAULT_SIZE, 221, Short.MAX_VALUE))
 					.addGap(157))
 				.addGroup(Alignment.LEADING, gl_contentPane.createSequentialGroup()
 					.addGap(263)
 					.addComponent(regBtn)
 					.addContainerGap(288, Short.MAX_VALUE))
 		);
 		gl_contentPane.setVerticalGroup(
 			gl_contentPane.createParallelGroup(Alignment.LEADING)
 				.addGroup(gl_contentPane.createSequentialGroup()
 					.addGap(61)
 					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
 						.addComponent(iUsername)
 						.addComponent(username, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE))
 					.addGap(18)
 					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
 						.addComponent(iPassword)
 						.addComponent(pass, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE))
 					.addGap(18)
 					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
 						.addComponent(iFullname)
 						.addComponent(fullname, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE))
 					.addGap(18)
 					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
 						.addComponent(iEmail)
 						.addComponent(email, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE))
 					.addGap(18)
 					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
 						.addComponent(iPhone)
 						.addComponent(phone, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
 					.addGap(55)
 					.addComponent(regBtn)
 					.addContainerGap(73, Short.MAX_VALUE))
 		);
 		contentPane.setLayout(gl_contentPane);
        regBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                String uname = username.getText();
                String pw = new String(pass.getPassword());
                String fname = fullname.getText();
                String eml = email.getText();
                String nphone = phone.getText();
                if (controller.register(uname, pw, fname, eml, nphone)){
                    JOptionPane.showMessageDialog(new JFrame(), "Created!!");
                }   else JOptionPane.showMessageDialog(new JFrame(), "Username already exist!!");

            }
        });
        
        ImageIcon background=new ImageIcon("others.jpg");
	    Image img=background.getImage();
	    Image temp=img.getScaledInstance(625,474,Image.SCALE_SMOOTH);
	    background=new ImageIcon(temp);
	    JLabel back=new JLabel(background);
	    back.setLayout(null);
	    back.setBounds(0,0,625,474);
		contentPane.add(back);

       
    }
}
