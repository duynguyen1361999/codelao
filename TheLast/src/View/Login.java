package View;

import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

import Connector.Connect;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.beans.Statement;
import java.sql.Connection;
import java.sql.ResultSet;

public class Login extends JFrame {

	private JPanel contentPane;
	private JTextField txtUsername;
	private JPasswordField txtPassword;
	private JLabel title;
	private JLabel username;
	private JLabel password;
	private JButton btnLogin;
	private static int id;

    public static int getId()
    {
        return id;
    }

    public  static  void setId(int _id)
    {
        id = _id;
    }

    public  Login(){

    	setTitle("Login");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 560, 390);
		
		setLocationRelativeTo(null);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		title = new JLabel("My Store");
		title.setBackground(Color.BLACK);
		title.setFont(new Font("Tahoma", Font.BOLD, 24));
		title.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(title);
		
		username = new JLabel("Username");
		username.setFont(new Font("Tahoma", Font.PLAIN, 16));
		username.setHorizontalAlignment(SwingConstants.CENTER);
		
		password = new JLabel("Password");
		password.setHorizontalAlignment(SwingConstants.CENTER);
		password.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		btnLogin = new JButton("Login");
		btnLogin.setForeground(Color.BLUE);
		btnLogin.setBackground(Color.WHITE);
		btnLogin.setFont(new Font("Tahoma", Font.PLAIN, 13));
		
		contentPane.add(username);
        contentPane.add(password);
        contentPane.add(btnLogin);
        

		
		ImageIcon background=new ImageIcon("login.jpg");
	    Image img=background.getImage();
	    Image temp=img.getScaledInstance(550,380,Image.SCALE_SMOOTH);
	    background=new ImageIcon(temp);
	    JLabel back=new JLabel(background);
	    back.setLayout(null);
	    back.setBounds(0,0,550,380);
		contentPane.add(back);
		
		
		
		
		Border border = BorderFactory.createLineBorder(Color.BLACK, 2);

		
		txtUsername = new JTextField();
		txtUsername.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtUsername.setColumns(10);
		txtUsername.setBorder(border);
		
		
		
		
		
		txtPassword = new JPasswordField();
		txtPassword.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtPassword.setBorder(border);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(201)
							.addComponent(title, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(101)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
								.addComponent(password, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(username, GroupLayout.DEFAULT_SIZE, 80, Short.MAX_VALUE))
							.addGap(30)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
								.addComponent(txtPassword)
								.addComponent(txtUsername, GroupLayout.PREFERRED_SIZE, 178, GroupLayout.PREFERRED_SIZE)))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(220)
							.addComponent(btnLogin)))
					.addContainerGap(145, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(39)
					.addComponent(title, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
					.addGap(56)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(username, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
						.addComponent(txtUsername, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(txtPassword, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
						.addComponent(password))
					.addGap(28)
					.addComponent(btnLogin)
					.addContainerGap(84, Short.MAX_VALUE))
		);
		contentPane.setLayout(gl_contentPane);
        btnLogin.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionEvent) {
                String user = txtUsername.getText();
                String pass = new String(txtPassword.getPassword());

                try {
                    Connect conn = new Connect();
                    String sql = "SELECT * FROM user";
                    ResultSet res = conn.creQuery(sql);
                    boolean flag = false;
                    while (res.next())
                    {
                        String usernameDB = res.getString("username");
                        String passwordDB = res.getString("password");

                        if (user.equals(usernameDB) && pass.equals(passwordDB))
                        {
                            flag = true;
                            break;
                        }
                    }

                    if (!flag){
                        System.out.println("Wrong!!!");
                        JOptionPane.showMessageDialog(new JFrame(), "incorrect Username or password");
                        return;
                    }
                    setId(res.getInt("id"));



                    if (res.getInt("permission") == 0)
                    {
                        UserView userView = new UserView();
                        Login log = new Login();
                        		
                        userView.setVisible(true);
                        userView.setLocationRelativeTo(null);
                        log.setVisible(false);
                        
                    } else {
                        AdminView adminView = new AdminView();
                        Login log = new Login();
                        adminView.setVisible(true);
                        adminView.setLocationRelativeTo(null);
                        log.setVisible(false);
                    }

                    setVisible(false);
                } catch (Exception e)
                {
                    e.printStackTrace();
                }


            }
        });

        
        this.setContentPane(contentPane);

        this.setVisible(true);
    }
}
