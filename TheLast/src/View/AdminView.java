package View;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AdminView extends Viewer {
    AdminView(){
        super();
        this.setTitle("Admin View");

        JMenuItem admin = new JMenuItem("Admin");
        admin.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                AdminFrame adminFrame = new AdminFrame();
                
            }
        });

        menuBar.add(admin);
    }
}
