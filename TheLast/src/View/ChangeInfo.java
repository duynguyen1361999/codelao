package View;

import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import Controller.Controller;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ChangeInfo {
    ChangeInfo(){
        final Controller controller = new Controller();

        final JFrame jFrame = new JFrame();
       
        jFrame.setVisible(true);
       
        jFrame.setTitle("Change info");
        jFrame.setResizable(false);
        jFrame.setBounds(100, 100, 624, 418);
        jFrame.setLocationRelativeTo(null);
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		jFrame.setContentPane(contentPane);
		


        DefaultTableModel res = controller.infoAccount();

        final JTextField name = new JTextField(res.getValueAt(0,1).toString());
        name.setColumns(10);


        final JTextField email = new JTextField(res.getValueAt(0,2).toString());
        email.setColumns(10);


        final JTextField phone = new JTextField(res.getValueAt(0,3).toString());
        phone.setColumns(10);

        
		
		JLabel iname = new JLabel("Name");
		iname.setFont(new Font("Tahoma", Font.PLAIN, 18));
		

		
		
		JLabel iphone = new JLabel("Phone");
		iphone.setFont(new Font("Tahoma", Font.PLAIN, 18));
		

		
		
		JLabel iemail = new JLabel("Email");
		iemail.setFont(new Font("Tahoma", Font.PLAIN, 18));
		

		
		
		final JButton change = new JButton("Change");
		change.setForeground(Color.BLUE);
		change.setFont(new Font("Tahoma", Font.PLAIN, 18));
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(123)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
							.addGroup(gl_contentPane.createSequentialGroup()
								.addComponent(iname, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE)
								.addGap(30))
							.addGroup(gl_contentPane.createSequentialGroup()
								.addComponent(iphone, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addPreferredGap(ComponentPlacement.RELATED)))
						.addComponent(iemail, GroupLayout.PREFERRED_SIZE, 57, GroupLayout.PREFERRED_SIZE))
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(4)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
								.addComponent(phone)
								.addComponent(name, GroupLayout.DEFAULT_SIZE, 271, Short.MAX_VALUE)))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(email)))
					.addContainerGap(114, Short.MAX_VALUE))
				.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
					.addContainerGap(268, Short.MAX_VALUE)
					.addComponent(change)
					.addGap(245))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(73)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(iname)
						.addComponent(name, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE))
					.addGap(31)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(phone, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
						.addComponent(iphone))
					.addGap(36)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(iemail)
						.addComponent(email, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE))
					.addGap(43)
					.addComponent(change)
					.addContainerGap(63, Short.MAX_VALUE))
		);
		contentPane.setLayout(gl_contentPane);

        change.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                String nameAT = name.getText();
                String emailAT = email.getText();
                String phoneAT = phone.getText();
                if (controller.changeInfo(nameAT, emailAT, phoneAT))
                {
                    JOptionPane.showMessageDialog(new JFrame(), "Done");

                    jFrame.setVisible(false);
                } else JOptionPane.showMessageDialog(new JFrame(), "Error");
            }
        });
        
        ImageIcon background=new ImageIcon("others.jpg");
	    Image img=background.getImage();
	    Image temp=img.getScaledInstance(614,408,Image.SCALE_SMOOTH);
	    background=new ImageIcon(temp);
	    JLabel back=new JLabel(background);
	    back.setLayout(null);
	    back.setBounds(0,0,614,408);
		contentPane.add(back);
        
    }
}
