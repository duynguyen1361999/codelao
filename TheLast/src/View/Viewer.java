package View;

import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import Controller.Controller;

import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.net.URISyntaxException;
import java.sql.ResultSet;

public class Viewer extends JFrame {
    protected Controller controller = new Controller();
    protected JMenuBar menuBar;
   
    Viewer(){
//       this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    	setResizable(false);
        
        
        

        
        menuBar = new JMenuBar();

        JMenu account = new JMenu("Account");
        JMenu about = new JMenu("About");
        

        JMenuItem info = new JMenuItem("Info");
        info.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {

                try{
                    DefaultTableModel res = controller.infoAccount();

                    JOptionPane.showMessageDialog(null, new JScrollPane(new JTable(res)));

                } catch (Exception e)
                {
                    e.printStackTrace();
                }

            }
        });
        account.add(info);

        JMenuItem changeInfo = new JMenuItem("Change Info");
        changeInfo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                new ChangeInfo();
            }
        });
        account.add(changeInfo);

        JMenuItem changePassword = new JMenuItem("Change password");
        changePassword.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                ChangePass changePass = new ChangePass();
            }
        });
        
        JMenuItem logout = new JMenuItem("Log out");
        logout.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
            	
            	Login log = new Login();
            	log.setVisible(true);
            	setVisible(false);
            }
        });

        JMenuItem exit = new JMenuItem("Exit");
        exit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                System.exit(0);
            }
        });
        
        JMenuItem product = new JMenuItem("About production");
        product.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				new About();
				
			}
		});
        
       
        
        
        account.add(changePassword);
        account.add(logout);
        account.add(exit);
        about.add(product);
        
        menuBar.add(account);
        menuBar.add(about);
        
        this.setJMenuBar(menuBar);
        setBounds(100, 100, 481, 508);
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		
		
		JButton createBill = new JButton("Create a new bill");
		createBill.setFont(new Font("Tahoma", Font.PLAIN, 18));
		
		JButton history = new JButton("History");
		history.setFont(new Font("Tahoma", Font.PLAIN, 18));
		
		JButton addThuoc = new JButton("Add med");
		addThuoc.setFont(new Font("Tahoma", Font.PLAIN, 18));
		
		JButton addLo = new JButton("Add plot");
		addLo.setFont(new Font("Tahoma", Font.PLAIN, 18));
		
		JButton medTable = new JButton("Med table");
		medTable.setFont(new Font("Tahoma", Font.PLAIN, 18));
		
		JButton checkStorage = new JButton("Check Storage");
		checkStorage.setFont(new Font("Tahoma", Font.PLAIN, 18));
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(160)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addComponent(checkStorage, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 167, Short.MAX_VALUE)
						.addComponent(addLo, GroupLayout.DEFAULT_SIZE, 167, Short.MAX_VALUE)
						.addComponent(addThuoc, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 167, Short.MAX_VALUE)
						.addComponent(history, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 167, Short.MAX_VALUE)
						.addComponent(createBill, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(medTable, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 167, Short.MAX_VALUE))
					.addGap(147))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(45)
					.addComponent(medTable)
					.addGap(38)
					.addComponent(createBill)
					.addGap(36)
					.addComponent(history)
					.addGap(36)
					.addComponent(addThuoc)
					.addGap(36)
					.addComponent(addLo)
					.addGap(34)
					.addComponent(checkStorage)
					.addContainerGap(36, Short.MAX_VALUE))
		);
		contentPane.setLayout(gl_contentPane);

        

//        JPanel contenJPanel = new JPanel();
//        JButton createBill = new JButton("Create new Bill");
		
		medTable.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				new CheckMed();
				
			}
		});
		
        createBill.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
            	
            	new CreateBill();
//            	Viewer view = new Viewer();
//                view.setEnabled(false);
                
            }
        });
//        contenJPanel.add(createBill);

//        JButton checkLog = new JButton("Check Log");
        	history.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                new History();
            }
        });
//        contenJPanel.add(checkLog);

//        JButton addThuoc = new JButton("Add Thuoc");
        addThuoc.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                new AddThuoc();
            }
        });
//        contenJPanel.add(addThuoc);

//        JButton addLo = new JButton("Add Lo");
        addLo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                new AddLo();
            }
        });
        checkStorage.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				new CheckStorage();
				
			}
		});
        
        ImageIcon background=new ImageIcon("others.jpg");
	    Image img=background.getImage();
	    Image temp=img.getScaledInstance(471,498,Image.SCALE_SMOOTH);
	    background=new ImageIcon(temp);
	    JLabel back=new JLabel(background);
	    back.setLayout(null);
	    back.setBounds(0,0,471,498);
		contentPane.add(back);
		
		
//        contenJPanel.add(addLo);

//        this.setContentPane(contenJPanel);
    }
}
