package View;

import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.border.EmptyBorder;

import Controller.Controller;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ChangePass extends JFrame {
    Controller controller = new Controller();
    public ChangePass(){


    	this.setTitle("Change password");
    	this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setBounds(100, 100, 560, 390);
		setLocationRelativeTo(null);
		setResizable(false);
		JPanel contentPane = new JPanel();
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		JLabel labelNewPass;
		JLabel labelCfPass ;
		JPasswordField passwordField;
		JPasswordField confirmPass;
		JButton confirm ;
		
		
		labelNewPass = new JLabel("New Pass :");
		labelNewPass.setFont(new Font("Tahoma", Font.PLAIN, 16));
		labelNewPass.setHorizontalAlignment(SwingConstants.CENTER);
		
		passwordField = new JPasswordField();
		passwordField.setFont(new Font("Tahoma", Font.PLAIN, 15));
		passwordField.setColumns(10);
		
		labelCfPass = new JLabel("Confirm Pass :");
		labelCfPass.setHorizontalAlignment(SwingConstants.CENTER);
		labelCfPass.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		confirm= new JButton("confirm");
		confirm.setForeground(Color.BLUE);
		confirm.setBackground(Color.WHITE);
		confirm.setFont(new Font("Tahoma", Font.PLAIN, 13));
		
		confirmPass = new JPasswordField();
		confirmPass.setFont(new Font("Tahoma", Font.PLAIN, 15));
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(201)
							)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(101)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
								.addComponent(labelCfPass, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(labelNewPass, GroupLayout.DEFAULT_SIZE, 80, Short.MAX_VALUE))
							.addGap(30)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
								.addComponent(confirmPass)
								.addComponent(passwordField, GroupLayout.PREFERRED_SIZE, 178, GroupLayout.PREFERRED_SIZE)))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(220)
							.addComponent(confirm)))
					.addContainerGap(145, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(39)
					
					.addGap(56)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(labelNewPass, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
						.addComponent(passwordField, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(confirmPass, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
						.addComponent(labelCfPass))
					.addGap(28)
					.addComponent(confirm)
					.addContainerGap(84, Short.MAX_VALUE))
		);
		contentPane.setLayout(gl_contentPane);

        confirm.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {

                String newPass = new String(passwordField.getPassword());
                String cfPass = new String(confirmPass.getPassword());

                String news = "Confirmation mismatched";

                // phần này đang sida không lên được label
                if (controller.changePass(newPass, cfPass))
                {
                    news = "Done!!";
                }

                JOptionPane.showMessageDialog(new JFrame(), news);
                System.out.println(news);
//                contenPanel.add(label);
            }
        });
        
        ImageIcon background=new ImageIcon("others.jpg");
	    Image img=background.getImage();
	    Image temp=img.getScaledInstance(550,380,Image.SCALE_SMOOTH);
	    background=new ImageIcon(temp);
	    JLabel back=new JLabel(background);
	    back.setLayout(null);
	    back.setBounds(0,0,550,380);
		contentPane.add(back);

        this.setContentPane(contentPane);
        this.setAlwaysOnTop (true);
        this.setVisible(true);
    }
}
