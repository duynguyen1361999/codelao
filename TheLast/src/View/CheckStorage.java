package View;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import Controller.Controller;

public class CheckStorage {
	Controller controller = new Controller();
    CheckStorage(){
        DefaultTableModel res = controller.checkStorage();

        JOptionPane.showMessageDialog(null, new JScrollPane(new JTable(res)));
    }
}
