package View;

import javax.swing.*;

public class About {
    public About() {
        Runnable r = () -> {
            String html = "<html><body width='%1s'><h1>About Production</h1>"
                    + "<p>This is our first production which is a simple "
                    + "homework. It's a medicine store management program."
                    + "This is first version for it.";

            // change to alter the width
            int w = 400;

            JOptionPane.showMessageDialog(null, String.format(html, w, 500));
        };
        SwingUtilities.invokeLater(r);
    }
}

