-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 30, 2018 at 06:20 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `medicinestore`
--

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `MaHoaDon` int(11) NOT NULL,
  `dsthuoc` varchar(200) NOT NULL,
  `tien` int(11) NOT NULL,
  `ngay` date NOT NULL,
  `customer` varchar(50) NOT NULL,
  `idU` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `history`
--

INSERT INTO `history` (`MaHoaDon`, `dsthuoc`, `tien`, `ngay`, `customer`, `idU`) VALUES
(36, ' 2', 150000, '2018-12-01', 'NguyenDuy', 1);

-- --------------------------------------------------------

--
-- Table structure for table `lothuoc`
--

CREATE TABLE `lothuoc` (
  `MaLo` varchar(10) COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `MaThuoc` int(11) NOT NULL,
  `NgayNhap` date NOT NULL,
  `SoLuong` int(11) NOT NULL,
  `HSD` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;

--
-- Dumping data for table `lothuoc`
--

INSERT INTO `lothuoc` (`MaLo`, `MaThuoc`, `NgayNhap`, `SoLuong`, `HSD`) VALUES
('BK301', 6, '2018-11-24', 1966, '2018-11-23'),
('DM901', 1, '2030-10-25', 89, '2018-11-23'),
('HP146', 5, '2018-11-14', 29989, '2018-11-13'),
('LA894', 12, '2017-11-14', 40, '2019-04-18'),
('LD215', 2, '2019-04-19', 15, '2018-05-07'),
('NY968', 3, '2018-12-18', 23, '2017-12-11'),
('PR965', 4, '2019-03-01', 0, '2018-11-29'),
('XL308', 10, '2018-02-25', 900, '2019-10-29');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `id` int(11) NOT NULL DEFAULT '-1',
  `name` varchar(50) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `email` varchar(30) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `phonenumber` varchar(20) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`id`, `name`, `email`, `phonenumber`) VALUES
(1, 'TruongHoangGiang', 'giang123@gmail.com', '0396123459'),
(2, 'NguyenAnhDuy', 'duy456@gmail.com', '0356485648'),
(3, 'NguyenVietHoang', 'hoang789@gmail.com', '0983452316');

-- --------------------------------------------------------

--
-- Table structure for table `thuoc`
--

CREATE TABLE `thuoc` (
  `MaThuoc` int(11) NOT NULL,
  `TenThuoc` varchar(30) COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `LoaiThuoc` varchar(30) COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `DonVi` varchar(10) COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `GiaTien` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;

--
-- Dumping data for table `thuoc`
--

INSERT INTO `thuoc` (`MaThuoc`, `TenThuoc`, `LoaiThuoc`, `DonVi`, `GiaTien`) VALUES
(1, 'Aspirin', 'thuốc viên', 'lọ', 23000),
(2, 'Lomustine', 'viên con nhộng', 'hộp', 30000),
(3, 'Salonpas', 'miếng dán', 'hộp', 20000),
(4, 'Flucinar', 'thuốc bôi', 'tuýp', 15000),
(5, 'Coldi-B', 'thuốc xịt', 'lọ', 23000),
(6, 'Calcium', 'thực phẩm chức năng', 'hộp', 100000),
(10, 'Berberin', 'thuốc viên', 'lọ', 2000),
(12, 'Boganic', 'thu?c viên', 'v?', 10000);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(30) COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `password` varchar(30) COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `permission` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `permission`) VALUES
(1, 'giang2k', '123123', 1),
(2, 'duy2k1', '123456', 0),
(3, 'hoang2k4', '654321', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`MaHoaDon`);

--
-- Indexes for table `lothuoc`
--
ALTER TABLE `lothuoc`
  ADD PRIMARY KEY (`MaLo`),
  ADD KEY `MaThuoc` (`MaThuoc`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `thuoc`
--
ALTER TABLE `thuoc`
  ADD PRIMARY KEY (`MaThuoc`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `MaHoaDon` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `lothuoc`
--
ALTER TABLE `lothuoc`
  MODIFY `MaThuoc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `thuoc`
--
ALTER TABLE `thuoc`
  MODIFY `MaThuoc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `lothuoc`
--
ALTER TABLE `lothuoc`
  ADD CONSTRAINT `lothuoc_ibfk_1` FOREIGN KEY (`MaThuoc`) REFERENCES `thuoc` (`MaThuoc`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `staff`
--
ALTER TABLE `staff`
  ADD CONSTRAINT `id` FOREIGN KEY (`id`) REFERENCES `user` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
